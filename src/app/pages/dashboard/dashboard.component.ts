import { Component } from '@angular/core';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {

  partidos: any = []

  fotos: any = []

  grafica: string = 'barras'

  constructor() {
    this.partidos = [
      {
        nombre: 'PAN',
        votos: 0,
      },
      {
        nombre: 'PRD',
        votos: 0,
      },
      {
        nombre: 'Morena',
        votos: 0,
      },
      {
        nombre: 'PRI',
        votos: 0,
      },
      {
        nombre: 'Movimiento Naranja',
        votos: 0,
      },
    ]
    setInterval(() => {
      this.updateResults()
    }, 2000)

    setInterval(() => {
      this.updateFotos()
    }, 14840)
  }

  updateResults() {
    const position = Math.floor(Math.random() * 4) + 1

    const randomVotes = Math.floor(Math.random() * 30) + 1

    this.partidos[position].votos += randomVotes

    this.partidos = this.partidos.sort((a, b) => b.votos - a.votos)

  }

  updateFotos() {
    const position = Math.floor(Math.random() * 4) + 1
    this.fotos.unshift({
      name: 'Casilla ' + position,
      date: new Date(),
    })
  }

  estados = [{
    'Estado': 'TODOS',
    'Abreviacion': '-',
  },
  {
    'Estado': 'Aguascalientes',
    'Abreviacion': 'AGS',
  },
  {
    'Estado': 'Baja California',
    'Abreviacion': 'BC',
  },
  {
    'Estado': 'Baja California Sur',
    'Abreviacion': 'BCS',
  },
  {
    'Estado': 'Campeche',
    'Abreviacion': 'CAMP',
  },
  {
    'Estado': 'Chiapas',
    'Abreviacion': 'CHIS',
  },
  {
    'Estado': 'Chihuahua',
    'Abreviacion': 'CHIH',
  },
  {
    'Estado': 'Coahuila',
    'Abreviacion': 'COAH',
  },
  {
    'Estado': 'Colima',
    'Abreviacion': 'COL',
  },
  {
    'Estado': 'Distrito Federal',
    'Abreviacion': 'DF',
  },
  {
    'Estado': 'Durango',
    'Abreviacion': 'DGO',
  },
  {
    'Estado': 'Guanajuato',
    'Abreviacion': 'GTO',
  },
  {
    'Estado': 'Guerrero',
    'Abreviacion': 'GRO',
  },
  {
    'Estado': 'Hidalgo',
    'Abreviacion': 'HGO',
  },
  {
    'Estado': 'Jalisco',
    'Abreviacion': 'JAL',
  },
  {
    'Estado': 'Estado de México',
    'Abreviacion': 'MEX',
  },
  {
    'Estado': 'Michoacán',
    'Abreviacion': 'MICH',
  },
  {
    'Estado': 'Morelos',
    'Abreviacion': 'MOR',
  },
  {
    'Estado': 'Nayarit',
    'Abreviacion': 'NAY',
  },
  {
    'Estado': 'Nuevo León',
    'Abreviacion': 'NL',
  },
  {
    'Estado': 'Oaxaca',
    'Abreviacion': 'OAX',
  },
  {
    'Estado': 'Puebla',
    'Abreviacion': 'PUE',
  },
  {
    'Estado': 'Querétaro',
    'Abreviacion': 'QRO',
  },
  {
    'Estado': 'Quintana Roo',
    'Abreviacion': 'QR',
  },
  {
    'Estado': 'San Luis Potosí',
    'Abreviacion': 'SLP',
  },
  {
    'Estado': 'Sinaloa',
    'Abreviacion': 'SIN',
  },
  {
    'Estado': 'Sonora',
    'Abreviacion': 'SON',
  },
  {
    'Estado': 'Tabasco',
    'Abreviacion': 'TAB',
  },
  {
    'Estado': 'Tamaulipas',
    'Abreviacion': 'TAMS',
  },
  {
    'Estado': 'Tlaxcala',
    'Abreviacion': 'TLAX',
  },
  {
    'Estado': 'Veracruz',
    'Abreviacion': 'VER',
  },
  {
    'Estado': 'Yucatán',
    'Abreviacion': 'YUC',
  },
  {
    'Estado': 'Zacatecas',
    'Abreviacion': 'ZAC',
  },
]



}
