import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

let counter = 0;

@Injectable()
export class UserService {

  private users = {
    nick: { name: 'Oscar Carrasco', picture: 'assets/images/nick.png' },
    eva: { name: 'Xilone Araiza', picture: 'assets/images/eva.png' },
    jack: { name: 'Cedric Luckie', picture: 'assets/images/jack.png' },
    lee: { name: 'Natalia Knapp', picture: 'assets/images/lee.png' },
    alan: { name: 'Julio Planas', picture: 'assets/images/alan.png' },
    kate: { name: 'Marco Antonio Maldonado', picture: 'assets/images/kate.png' },
  };

  private userArray: any[];

  constructor() {
    // this.userArray = Object.values(this.users);
  }

  getUsers(): Observable<any> {
    return Observable.of(this.users);
  }

  getUserArray(): Observable<any[]> {
    return Observable.of(this.userArray);
  }

  getUser(): Observable<any> {
    counter = (counter + 1) % this.userArray.length;
    return Observable.of(this.userArray[counter]);
  }
}
